linenoise (1.0+git20240223.d895173d679b-1) UNRELEASED; urgency=medium

  * New upstream version 1.0+git20240223.d895173d679b
  * d/control: add `Multi-Arch:` fields
  * d/control: bump Standards-Version to 4.7.0
  * d/control, d/copyright: update my e-mail address
  * d/copyright: use BSD-2 like upstream
  * d/gbp.conf: white-space fix
  * d/patches: add numerical prefixes to file-names

 -- Jeremy Sowden <azazel@debian.org>  Sat, 11 May 2024 15:55:10 +0100

linenoise (1.0+git20230327.93b2db9bd496-1) unstable; urgency=medium

  * Initial release (closes: #940960).
  * Drop makefile patch and add rules directly to d/rules.
  * Added d/watch.
  * Update patches to reflect those posted upstream.
  * d/patches: use variables for `install` and `ln`
  * d/rules: include /usr/share/dpkg/architecture.mk instead of calling dpkg-architecture directly.
  * d/rules: use variable for `make`
  * d/control: bump `debhelper-compat` dependency version to 13
  * d/control: bump Standards-Version to 4.6.2
  * d/control: set Rules-Requires-Root: no.
  * d/control: update Vcs-* URL's.
  * d/copyright: update dates for packaging and change licence to GPL-2+
  * d/copyright: update dates for linenoise.*
  * d/patches: convert to DEP-3
  * d/gbp.conf: DEFAULT: set `debian-branch`
  * d/gbp.conf: dch: set `commit-msg`
  * d/.gitignore: ignore packaging artefacts
  * d/.gitignore: ignore Emacs artefacts

 -- Jeremy Sowden <jeremy@azazel.net>  Wed, 12 Jul 2023 18:40:21 +0100
